package MapReduce;

import java.io.IOException;
import java.io.DataOutput;
import java.io.DataInput;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

/**
 * GeoTag is a class to store the city, metropolitan and country.
 * Format : (city, metropolitan, country)
 * @author Saif Charaniya
 *
 */
public class GeoTag implements Writable {
	
	private String city;
	private String metro;
	private String country;
	private Text txt = new Text();

	/**
	 * Main constructor
	 * @param city
	 * @param metro
	 * @param country
	 */
	public GeoTag(String city, String metro, String country) {
		
		this.city = city;
		this.metro = metro;
		this.country = country;
	}
	
	/**
	 * empty constructor
	 */
	public GeoTag() {
		
		this.city = "null";  //strings instead of null type for output purposes
		this.country= "null";
		this.metro = "null";
	}
	
	
	/**
	 * Setters
	 * @param city
	 * @param metro
	 * @param county
	 */
	public void set(String city, String metro, String county) {
		
		this.city = city;
		this.metro = metro;
		this.country = country;	
	}

	/**
	 * Hadoop IO writing 
	 */
	public void write(DataOutput out) throws IOException {
		
		txt.set(this.city);
		txt.write(out);
		
		txt.set(this.metro);
		txt.write(out);
		
		txt.set(this.country);
		txt.write(out);
	}
	
	/**
	 * Haoop IO reading
	 */
	public void readFields(DataInput in) throws IOException {
		
		
	}
	
	public String toString() {
		return "\"Location\": { \"city\": \"" +
				city + "\",  \"metro\": \"" + metro + "\",  \"country\": \"" +
				country + "\" }}";
	}
}
