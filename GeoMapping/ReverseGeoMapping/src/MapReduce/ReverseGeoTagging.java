package MapReduce;

import geocode.GeoName;
import geocode.ReverseGeoCode;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * ReverseGeoTagging reads in json objects of Instagram posts and converts their
 * location data (ie latitude and longitude) to a city, major center, and 
 * country, and saves this along with the post id. 
 * 
 * MapReduce keys: post id (unique)
 * 			 Values: 3 pairs of text: (city, metropolitan, country)
 * @author Saif Charaniya
 *
 */
public class ReverseGeoTagging extends Configured implements Tool{
	
	
	public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(),
        		new ReverseGeoTagging(), args);
        System.exit(res); 
    }
	
	/**
	 * Map a Instagram post to a new json object with id and location
	 * @author Saif Charaniya
	 *
	 */
	public static class JSONMapper
    extends Mapper<LongWritable, Text, Text, GeoTag>{
		
  
        @Override
        public void map(LongWritable key, Text value, Context context
                ) throws IOException, InterruptedException {
   
    		JsonNode data;
    		GeoName place;
    		ObjectMapper jsonMapper;
    		ReverseGeoCode geoCode;
    		InputStream citiesStream;
    		GeoTag geoTag = new GeoTag();

        	jsonMapper = new ObjectMapper();
        	
    		try {
    			//valid json object??
    			data = jsonMapper.readValue(
    					value.toString(), JsonNode.class);
    		}
    		catch (Exception e){
    			//nope, skip it
    			return;
    		}
        	
    		//post id in json form, completed by geoTag
        	String postId = "{ \"id\": \"";
        	postId = postId + data.get("id").asText();
        	postId = postId + "\",";
        	
        	
        	if (!data.get("location").toString().contains("latitude")) {
        		//no geo location to reverse ie not lat or long
        		context.write(new Text(postId), geoTag);
        		return;
        	}
        	
        	//try {
        	else {
        		//latitude and longitude exist!
        		//System.out.println(data.get("location"));
        		
        		//set geo reverse tree
        		citiesStream = this.getClass().getResourceAsStream("cities1000.txt"); 
            	geoCode = new ReverseGeoCode(citiesStream, true);
        		
        		//try {
        		double latitude = Double.parseDouble(data.get("location")
        				.get("latitude").toString());
        		
        		//System.out.println(latitude);
        		
        		double longitude = Double.parseDouble(data.get("location")
        				.get("longitude").toString());
        		      		
        		//System.out.println(longitude);
        		
        		
        		place = geoCode.nearestPlace(latitude, longitude);
        		geoTag = new GeoTag(place.name, place.major_centre,
        				place.country);
        		
        	//	System.out.println(place.name);
        		context.write(new Text(postId), geoTag);
        		
        		return;
        	}
        }
    }
	
	@Override
	public int run(String[] args0) throws Exception {
		
		//input and output folders for command line args
		String inputFolder = args0[0];
		//String geoListFile = args0[1];
		String outputFolder = args0[1];
		
		Configuration conf = this.getConf();
		
        Job job = Job.getInstance(conf, "Reverse Geo Tagging");
        job.setJarByClass(ReverseGeoTagging.class);
        
        job.setNumReduceTasks(0);
        
        job.setInputFormatClass(TextInputFormat.class);
        job.setMapperClass(JSONMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(GeoTag.class);
       
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(GeoTag.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        TextInputFormat.addInputPath(job, new Path(inputFolder));
        TextOutputFormat.setOutputPath(job, new Path(outputFolder));
 
        return job.waitForCompletion(true) ? 0 : 1;
	}

}
