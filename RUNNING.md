Welcome to my project!

Folders:

	GeoMapping - A Java project containing the MapReduce code and reverse geo library.
	
	analysis - A folder containing the visualizations created for the project in subfolders by analysis section.  There are many Python scripts and a few R scripts, along with the data used by them.  The data, aka csv or txt files, are all outputted by Spark Python code on the cluster.
	
	instagram_data - Contains the Python code to allow one to download and save Instagram posts and user data.  I have removed my developer ID in the code here.
	
	paper - contains the final paper and project summary.
	
	spark_code - contains all the Python Spark implementation used in the project. More on how to run them bellow.
	
	webfront - contains the webfiles that are duplicated on my webspace in the SFU cluster. You can visit the website at https://goo.gl/KZpG9T.  A short google url allows me to track the usage of the webpage.
	
Spark Code!
	
	In the spark_code folder there are Python scripts used in the analysis. Each file has a header explaining what it does (or is supposed to do (they all work!) ). Bellow are the commands to run the files.  Since there are many ways to run the files (ie # of executors, # of nodes, ect), the bellow commands only contain the command line arguments required to run the file on the cluster, (assuming I'm using my own account).  Some of the Python files have no inputs (because they were hard coded).  In this case you may need to edit those hard coded locations.  All hard coding of locations is done in the main block (just scroll to the bottom).  The resason for this is that I ran some of the code on my laptop when the cluster was busy.
	
	Generic file locations:
	raw_posts_json : where the raw Instagram data is. located at /user/saifc/big_files/
	output_folder: any folder where you wish to see the output outputted to.
	FINAL_PROJECT_PARQUET: the location for the parquet files on the cluster.  Located at /user/saifc/FINAL_PROJECT_PARQUET/
	
	cd spark_code
	
	1.
		$get_list_of_users.py raw_posts_json output_folder
		
	2.
		$combine_jsons.py
		
	3.
		$get_toronto_posts.py
		
	4.
		$brand_value.py
		
	5.
		$gen_stats/city_aggregates.py
		
	6.
		$gen_stats/country_aggregates.py
		
	7.
		$gen_stats/metro_aggregates.py
		
	
	FIN
	
	All Python code in the analysis section have hard coded resource locations.  To run a file just use: $python <file>

Files on the cluster: 

I have stored numerous files on the cluster and here are the locations and descriptions for each.

	/user/saifc/big_files/ : all raw Instagram posts (only the posts no user data or geo data)
	
	/user/saifc/FINAL_PROJECT_PARQUET/ : the location of the combined parquet files as described in the project report
	
	/user/saifc/user_data : raw user data of Instagram Users
	
	/user/saifc/geo_tags_1 : reverse geo tags of all the Instagram posts
	
Other output files and folders exists (and are too lengthy to explain here)
	
Thank you for the wonderful semester.
-Saif
	
	
	