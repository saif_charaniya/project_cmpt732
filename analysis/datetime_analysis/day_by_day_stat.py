from numpy import *
from pylab import *
from scipy.signal import *

def norm_ar(ar):
    """ Return normalized array"""

    norm = max(ar) - min(ar)
    rt = (ar - min(ar) ) / (1.0 * norm)
    return rt

#file location
daily_url = "full_year_data.txt"

#read data
dtype = [('date', 'S20'), ('count', float), ('likes', float), \
             ('comments', float),('pp', float),]
data = genfromtxt(daily_url, delimiter=";", dtype=dtype)

tofloat = vectorize(lambda i: float(i))
toSmallDate = vectorize(lambda i: i.split('-')[1] + ":" + i.split('-')[2])

#sort by date in file
data = sort(data, order='date')
data = data[1:]

#separate content
dates = data['date']
counts = data['count']
likes = data['likes']
pp = data['pp']
comments = data['comments']

#normalize counts, likes, pp
norm_counts = norm_ar(counts)
norm_likes = norm_ar(likes)
norm_pp = norm_ar(pp)
norm_comments = norm_ar(comments)

#array to show first and last date
plot_date = [0, len(dates) -1]
plot_date_value = [dates[0], dates[-1]]

###plot of all unfiltered
##figure()
##ax = gca()
##plot(norm_counts, label="Counts")
##plot(norm_likes, label="Likes")
##plot(norm_pp, label="PP")
##plot(norm_comments, label="Comments")
##xticks(plot_date, plot_date_value)
##ax.yaxis.set_visible(False)
##legend(loc=0)


#individual filtered plots
##figure()
##plot(norm_counts, 'b', linewidth=3)
counts_filtered = wiener(norm_counts,mysize=11, )
##ax = gca()
##xticks(plot_date, plot_date_value)
##ax.yaxis.set_visible(False)
##plot(x, 'r')

#filtering likes
##figure()
##plot(norm_likes, 'b', linewidth=3)
likes_filtered = wiener(norm_likes,mysize=11, )
##ax = gca()
##plot(x, 'r')
##
###filtering pp
##figure()
##plot(norm_pp, 'b', linewidth=3)
pp_filtered = wiener(norm_pp,mysize=13, )
##ax = gca()
##ax.yaxis.set_visible(False)
##plot(x, 'r')
##
###filtering comments
##figure())
##plot(norm_comments, 'b', linewidth=3)
comments_filtered = wiener(norm_comments, mysize=5, )
##ax = gca()
##ax.yaxis.set_visible(False)
##plot(x, 'r')

#plot of all filtered
figure()
ax = gca()
plot(counts_filtered, label="Posts")
plot(likes_filtered, label="Likes")
#plot(pp_filtered, label="PP")
plot(comments_filtered, label="Comments")
ax.text(263, 0.98, 'Solstice')
ax.text(183, 0.97, 'Easter')
ax.text(92, 0.66, 'Valentines\nDay')
ylim(0, 1.1)
xticks(plot_date, plot_date_value)
ax.yaxis.set_visible(False)
legend(loc=0)
show()
