# plot a bar graph of metro aggregates

from numpy import *
from pylab import *

def norm_ar(ar):
    """ Return normalized array"""

    norm = max(ar) - min(ar)
    rt = (ar - min(ar) ) / (1.0 * norm)
    return rt
 
file_url = "city_list.txt"
dtype = [('city', 'S40'), ('N', int), ('L', float), ('C', float),]
data = genfromtxt(file_url, delimiter=";", dtype=dtype)

c1 = data['city']
data.sort(order='N') #city list is unsorted :(

#split data
vals = data['N']
cities = data['city']
likes = data['L']
comments = data['C']

num = 20

#take top 20 only
top_values = vals[-num:]
top_cities = cities[-num:]
top_likes = likes[-num:]
top_com = comments[-num:]

#z norm likes, vals, comments
top_values = norm_ar(top_values)
top_likes = norm_ar(top_likes)
top_com = norm_ar(top_com)

top_cities[19] = "Uskudar"
top_cities[2] = 'Atasehir'

#plot 
#figure()
fig, ax = subplots()
pos = arange(num)
width = 0.3
barh(pos, top_values, width,label="Number of Posts", color='red')
barh(pos + width, top_likes, width, label="Likes", color='green')
barh(pos + 2*width, top_com, width, label="Comments", color='yellow')
legend(loc=0)
ax.set(yticks=pos + width, yticklabels=top_cities, ylim=[2*width - 1, num])
xlim(0,1.2)
ax.xaxis.set_visible(False)
#yticks(pos + 0.5, top_cities)                                               
title("Instagram Posts by Proper City")               
#grid(True)                                                                 
#savefig("all_country.png")                                           
show()  
