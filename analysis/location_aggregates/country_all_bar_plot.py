# plot a bar graph of country aggregates

from numpy import *
from pylab import *

def norm_ar(ar):
    """ Return normalized array"""

    norm = max(ar) - min(ar)
    rt = (ar - min(ar) ) / (1.0 * norm)
    return rt
 
file_url = "country_list.txt"
dtype = [('country', 'S40'), ('N', int), ('L', float), ('C', float),]
data = genfromtxt(file_url, delimiter=";", dtype=dtype)

codes = genfromtxt("country_codes.csv", delimiter=",", dtype=str, skip_header=1)
codes_cont = codes[:][:,0]
codes = codes[:][:,1]

data.sort(order='N')

#split data
vals = data['N']
countries = data['country']
likes = data['L']
comments = data['C']

num = 20

#take top 20 only
top_values = vals[-num:]
top_countries = countries[-num:]
top_likes = likes[-num:]
top_com = comments[-num:]

#norm likes, vals, comments
top_values = norm_ar(top_values)
top_likes = norm_ar(top_likes)
top_com = norm_ar(top_com)


#find country proper names
proper_countries = []
for tm in top_countries:
    index = where(codes==tm)[0][0]
    proper_countries.append(codes_cont[index])

top_countries = array(proper_countries, dtype=str)

###reverse list for plotting
##top_countries = top_countries[::-1]
##top_values = top_values[::-1]
##top_com = top_com[::-1]
##top_likes = top_likes[::-1]


#plot 
#figure()
fig, ax = subplots()
pos = arange(num)
width = 0.3
barh(pos, top_values, width,label="Number of Posts", color='red')
barh(pos + width, top_likes, width, label="Average Likes", color='green')
barh(pos + 2*width, top_com, width, label="Average Comments", color='yellow')
legend(loc=0)
ax.set(yticks=pos + width, yticklabels=top_countries, ylim=[2*width - 1, num])
xlim(0,1.2)
ax.xaxis.set_visible(False)
#yticks(pos + 0.5, top_cities)                                               
title("Instagram Posts by Country")               
#grid(True)                                                                 
#savefig("all_country.png")                                           
show()  
