# plot a bar graph of metro aggregates

from numpy import *
from pylab import *

def norm_ar(ar):
    """ Return normalized array"""

    norm = max(ar) - min(ar)
    rt = (ar - min(ar) ) / (1.0 * norm)
    return rt
 
file_url = "metro_list.txt"
dtype = [('metro', 'S40'), ('N', int), ('L', float), ('C', float),]
data = genfromtxt(file_url, delimiter=";", dtype=dtype)
get_metro = vectorize(lambda met: met.split("/")[1])

#split data
vals = data['N']
metros = data['metro']
likes = data['L']
comments = data['C']

num = 20

#remove metro begining
metros = get_metro(metros)

#take top 20 only
top_values = vals[:num]
top_metros = metros[:num]
top_likes = likes[:num]
top_com = comments[:num]

#z norm likes, vals, comments
top_values = norm_ar(top_values)
top_likes = norm_ar(top_likes)
top_com = norm_ar(top_com)

#reverse list for plotting
top_metros = top_metros[::-1]
top_values = top_values[::-1]
top_com = top_com[::-1]
top_likes = top_likes[::-1]

tm = []
for t in top_metros:
    tm.append(t.replace("_", " "))
top_metros = tm

#plot 
#figure()
fig, ax = subplots()
pos = arange(num)
width = 0.3
barh(pos, top_values, width,label="Number of Posts", color='red')
barh(pos + width, top_likes, width, label="Average Likes", color='green')
barh(pos + 2*width, top_com, width, label="Average Comments", color='yellow')
legend(loc=0)
ax.set(yticks=pos + width, yticklabels=top_metros, ylim=[2*width - 1, num])
xlim(0,1.2)
ax.xaxis.set_visible(False)
#yticks(pos + 0.5, top_metros)                                               
title("Instagram Posts by Metro")               
#grid(True)                                                                 
#savefig("all_country.png")                                           
show()  
