# Create a word cloud of the top hashtags used in Instagram chocolate posts.
# The top hashtags were found in Spark using FPGrowth

from os import path
from wordcloud import WordCloud
from numpy import *
import matplotlib.pyplot as plt
from PIL import Image

#read words and frequencies
dtype = [('words', 'S30'), ('freq', int),]
data = genfromtxt('fpgrowth_size1.txt', dtype=dtype, delimiter=";")

data = sort(data, order='freq')
words = data['words']
freq = data['freq']

#open image
image_mask = array(Image.open('kiss.png'))

#create tuples of words and frequenciese
tuples_freq = []
for w, f in zip(words, freq):
    tuples_freq.append((w,f))

#create wordcloud
wc = WordCloud(mask=image_mask, background_color='black').generate_from_frequencies(tuples_freq)

#plot
plt.imshow(wc)
plt.axis('off')
plt.show()

