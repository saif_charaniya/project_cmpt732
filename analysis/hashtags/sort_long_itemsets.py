# read a file with long itemsets (size 4 or more ideally) and sort the values in
# that itemset based on the individual item frequencies of each item in the
# itemset.  Save the sorted itemsets into a new file

from numpy import *

#long itemset file
lf = 'all_s4.txt'

#read words
dtype = [('words', 'S30'), ('freq', int),]
data = genfromtxt('fpgrowth_size1.txt', dtype=dtype, delimiter=";")

words = data['words'].tolist()
words_freq = data['freq'].tolist()

#new file
new_file = open('sorted_itemsets_long.csv', 'w')

#sequentiall read the long itemset file
with open(lf) as f:
    for line in f:

        #split line by ;
        splits = line.split(';')
        freq = splits[1]
        items = splits[0].split('-')
        items_freqs = []
    
        #for each item find the freq from words and freq
        for w in items:

            ind = words.index(w)
            items_freqs.append(-1 * words_freq[ind])

        #sort by items_freq

        sorted_items = [x for (y,x) in sorted(zip(items_freqs, items))]

        #create str to write to lf
        wrt_str = ""
        for w in sorted_items:
            wrt_str += w + "-"

        wrt_str = wrt_str[:-1]

        wrt_str += "," + freq 

        new_file.write(wrt_str)
        new_file.flush()

new_file.close()
            

        
