# plot 5 wordclouds, one for each country: us, br, it, au, ca

#imports
from os import path
from wordcloud import WordCloud
from numpy import *
import matplotlib.pyplot as plt
from PIL import Image

#read files, ';' delimited
dtype = [('words', 'S30'), ('freq', int),]

us_data = genfromtxt('us_s1.txt', dtype=dtype, delimiter=";")
br_data = genfromtxt('br_s1.txt', dtype=dtype, delimiter=";")
it_data = genfromtxt('it_s1.txt', dtype=dtype, delimiter=";")
au_data = genfromtxt('au_s1.txt', dtype=dtype, delimiter=";")
ca_data = genfromtxt('ca_s1.txt', dtype=dtype, delimiter=";")

#create tuples of (word, freq) to be used by wordcloud
us_tuples = []
for w, f in zip(us_data['words'], us_data['freq']):
    us_tuples.append((w,f))

br_tuples = []
for w, f in zip(br_data['words'], br_data['freq']):
    br_tuples.append((w,f))

it_tuples = []
for w, f in zip(it_data['words'], it_data['freq']):
    it_tuples.append((w,f))

au_tuples = []
for w, f in zip(au_data['words'], au_data['freq']):
    au_tuples.append((w,f))

ca_tuples = []
for w, f in zip(ca_data['words'], ca_data['freq']):
    ca_tuples.append((w,f))

#create wordclouds
us_wc = WordCloud().generate_from_frequencies(us_tuples)
br_wc = WordCloud().generate_from_frequencies(br_tuples)
it_wc = WordCloud().generate_from_frequencies(it_tuples)
au_wc = WordCloud().generate_from_frequencies(au_tuples)
ca_wc = WordCloud().generate_from_frequencies(ca_tuples)

#plot and show
plt.figure('USA')
plt.imshow(us_wc)
plt.axis('off')

plt.figure('BRAZIL')
plt.imshow(br_wc)
plt.axis('off')

plt.figure('ITALY')
plt.imshow(it_wc)
plt.axis('off')

plt.figure('AUSTRALIA')
plt.imshow(au_wc)
plt.axis('off')

plt.figure('CANADA')
plt.imshow(ca_wc)
plt.axis('off')


plt.show()
