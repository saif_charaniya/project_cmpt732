# read the single itemset file and create a file with words and corressponding
# random hex colours to be pasted in to js for the hierarchal d3 model

from numpy import *

#read words
dtype = [('words', 'S30'), ('freq', int),]
data = genfromtxt('fpgrowth_size1.txt', dtype=dtype, delimiter=";")

words = data['words'].tolist()

#new file
new_file = open('colors.txt', 'w')

for w in words:
    s = '\t' + '"' + w + '": ' + """'#'+Math.floor(Math.random()*16777215).toString(16)"""
    s += ",\n"
    new_file.write(s)
    new_file.flush()
new_file.close()
        

        
