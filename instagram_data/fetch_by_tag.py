##
##  This python program repeated downloads instagram posts that contain 
##  a specific hashtag. Posts will be downloaded continuously.
##
##  Instagram Posts will be saved into a multiple files each containing
##  a pre set threshold number of posts.  Endpoint tags for the posts are 
##  saved to a file which can allow the user to restart downloads easily
##
##  run by: "python fetch_by_tag.py".  No command line options available


import urllib2
import json
import sys
import time

## Edit these parameters as nessecary
TAG = ""
END_POINT_FILE = ""
OUTPUT_FILE_NAME = ""
OUTPUT_START_INDEX = 1
THRESHOLD = 300000

## Set Your Instagram API Parameters
ACCESS_TOKEN = ""
MAX_TAG_ID = 0 #set to 0 if new search


end_points = open(END_POINTS_FILE, 'a')

api_url = 'https://api.instagram.com/v1/tags/' + TAG
api_url = api_url + 'chocolate/media/recent?access_token=' + ACCESS_TOKEN
original_url = api_url

if (MAX_TAG_ID != 0):
    api_url = api_url + '&max_tag_id=' + MAX_TAG_ID

# HTTP Call to Instagram API
#
# URL -> URL to call
# objects -> number of curret objects in file
# max_tag -> current max_tag_id
# f -> file to save data set
#
# returns new url to call, number of object, and new max_tag_id
def get_media(url, objects, max_tag, f):
    """call instagram api with given url, and save to json file f 
     
    URL -> URL to call           
     objects -> number of curret objects in file
     max_tag -> current max_tag_id                                            
     f -> file to save data 
    
     returns new url to call, number of object, and new max_tag_id  """
    
    # save results of call
    try:
        response = urllib2.urlopen(url)
        html = response.read()
        data = json.loads(html)
        for post in data['data']:
            json.dump(post, f)
            f.write("\n")
            objects += 1
        f.flush()

    #IF the api returns a HTTP 429 Error, wait 1 hr before starting
    #Requests. 
    except urllib2.HTTPError, err:
        if err.code == 429:
            end_points.write("sleep: " + str(max_tag))
            end_points.write("\n")
            end_points.flush()
            time.sleep(3660)
            return (url, objects, max_tag)
        print ("Problem with URL")
        
    #get min tag id for next iteration
    try:
        new_id = data['pagination']['next_']
        new_url = original_url + '&max_tag_id=' + str(new_id)

        end_points.write(str(new_id))
        end_points.write("\n")
        end_points.flush()
        return (new_url, objects, new_id)

    except KeyError:
        print ("Error in data fetched")
        raise SystemExit


objects = 0
output = open(OUTPUT_FILE_NAME, 'w')
max_tag = MAX_TAG_ID
out_index = OUTPUT_START_INDEX

## Non stop loop to download content
while (api_url):

    api_url, objects, max_tag = get_media(new_url, objects, max_tag, output);
    if (objects > THRESHOLD):
        out_index += 1
        output.close()
        output = open(OUTPUT_FILE_NAME + str(out_index) + '.txt', 'w')
        objects = 0

end_points.close()
output.close()
