##
##  This program uses reads a file with user ids, and fetches the user's 
##  information using the Instagram Api. The user data is then saved to
##  multiple files in json format which can then be imported by Spark
##
## Change the parameters bellow as desired.


import json
import sys
import urllib2
import time
import numpy as np

## PARAMETERS
INPUT_FILE = "part-00000"
OUTPUT_FOLDER = "user_data"

## INSTAGRAM API PARAMETERS
ACCESS_TOKEN = '1731998280.96a6219.38780277d5a34c69a4fa21bb22fed266'

def get_user_details(uid, f):
    """Get user id and save to file specified"""
    
    url = base_url1 + uid + base_url2
    try:
        response = urllib2.urlopen(url)
        html = response.read()
        jo = json.loads(html)
        
        #f.write(user_data)
        json.dump(jo['data'], f)
        f.write("\n")
        f.flush()
        
    except urllib2.HTTPError, err:
    #except 
        if err.code == 429:
            time.sleep(3660)
            get_user_details(uid, f)

    except:
        get_user_details(uid, f)

# api call url
base_url1 = 'https://api.instagram.com/v1/users/'
base_url2 = '/?access_token=' + ACCESS_TOKEN

#read user ids
user_ids = np.loadtxt(INPUT_FILE, dtype=str)

#file setup
f = open(OUTPUT_FOLDER + "/users4.txt", 'w')
filestart = 4
objects = 0

i = len(user_ids)

for i in xrange(i):

    objects+= 1
    get_user_details(user_ids[i], f)
    
    #save multiple small files instead of one large file
    if (objects > 300000):
        filestart+= 1
        f.close()
        f = open(OUTPUT_FOLDER + "/users" + str(filestart) +".txt", 'w')
        objects = 0

f.close()
