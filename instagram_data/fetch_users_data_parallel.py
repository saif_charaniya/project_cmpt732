##                                                                             
##  This program uses reads a file with user ids, and fetches the user's       
##  information using the Instagram Api. The user data is then saved to        
##  multiple files in json format which can then be imported by Spark 
##                                
##  This parallel version allows for multiple access tokens and 
##  fetching data in parallel process (ie run program in multiple threads)
##
##  See bellow for command line args

import json
import sys
import urllib2
import time

##Command line args
start = int(sys.argv[1])  #user start index
end = int(sys.argv[2]) #user end index
prefix = sys.argv[3] #file prefix
pre_start = int(sys.argv[4]) #file number start
access_token = sys.argv[5] #access token for Instagram API


## PARAMETERS
INPUT_FILE = "part-00000"
OUTPUT_FOLDER = "user_data"

ACCESS_TOKEN = access_token

def get_user_details(uid, f):
    """Get user id and save to file specified"""
    
    url = base_url1 + uid + base_url2
    try:
        response = urllib2.urlopen(url)
        html = response.read()
        jo = json.loads(html)
        
        #f.write(user_data)
        json.dump(jo['data'], f)
        f.write("\n")
        f.flush()
    
    #handle possible http exceptions thrown by Instagram API or 
    #faulty network
    except urllib2.HTTPError, err:
    
        if err.code == 429:
            time.sleep(660)
            get_user_details(uid, f)
            
        elif err.code == 400:
            html = err.read()
            jo = json.loads(html)
            ster = jo.get('meta').get('error_type')
            msg = jo.get('meta').get('error_message')
            if (ster == 'APINotAllowedError'):
                print ('private user')

            elif (msg == 'this user does not exist'):
                print ('user dne')
            
            else:
                print (url)
                raise SystemExit

        else:
            print (err.code)
            print (url)
            time.sleep(600)
            get_user_details(uid, f)

    except:
        print (url)
        get_user_details(uid, f)

# api call url
base_url1 = 'https://api.instagram.com/v1/users/'
base_url2 = '/?access_token=' + ACCESS_TOKEN

#read user ids
text_ids = open(INPUT_FILE, "r")
user_ids = text_ids.readlines()

#file setup
f = open(OUTPUT_FOLDER + "/users" + prefix + str(pre_start), 'w')
filestart = pre_start
objects = 0

for i in xrange(start, end):

    print(i)
    objects+= 1
    get_user_details(user_ids[i].replace("\n", ""), f)

    if (objects > 300000):
        filestart+= 1
        f.close()
        f = open(OUTPUT_FOLDER + "/users" + prefix + str(filestart) +".txt",\
                     'w')
        objects = 0

f.close()
