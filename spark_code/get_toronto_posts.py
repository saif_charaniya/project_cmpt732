# This spark program get all the posts done in the metro toronto area.
# All that is required is latitude and longitude to be used in plotting
# later on

from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext


def main(parquet_location):

    #load spark
    conf = SparkConf().setAppName('Toronto Posts')
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)

    #read data
    data = sqlContext.read.parquet(parquet_location)

    #get needed columns
    rdd = data.select(data.metro, data.latitude, data.longitude).rdd.cache()

    #filter metro
    rdd = rdd.filter(lambda row: row.metro.lower() == "america/toronto")

    #extract lat long
    rdd = rdd.map(lambda row: str(row.latitude) + ","  + str(row.longitude))

    rdd = rdd.coalesce(1)

    #save to file
    rdd.saveAsTextFile("toronto")
    

if __name__ == "__main__":

    main("../FINAL_PROJECT_PARQUET") 
