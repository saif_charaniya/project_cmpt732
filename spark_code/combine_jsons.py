# 
# This file will combine three json object folders (posts, users, locations)
# into one parquet folder, filtering out attributes that are not needed.
# Instead on saving the output to 3 different parquet sets, (in order to reduce
# redundancy) they will be saved in a joined parquet, which will allow us to
# save time in performing joins, but which would also increase the memory size
# as a downfall.
#
#

from pyspark import SparkConf, SparkContext
import sys, operator
import json
from pyspark.sql import SQLContext
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DateType
from pyspark.sql import Row
from datetime import datetime

## Functions

def add_pairs ((a1, b1), (a2, b2)):
    """ Add two pairs of tuples by index """
    return (a1 + a2, b1 + b2)

def json_loads2(line):
    try:
        jo = json.loads(line)
        if type(jo) is dict:
            return (jo, 1)
    except :#ValueError:
        #print ("BADLINE:", line)
        return ("EMPTY", -1)

def error_jo (jo):
    try:
        jo.get('caption').get('created_time')
        return True
    except:
        #print ("ERRR")
        return False

def json_to_list(jo):
    """Take json object, return a list of wanted attributes."""
    
    rt = []

    rt.append(jo.get('id'))
    rt.append(long(jo.get('caption').get('created_time')))
    rt.append(long(jo.get('likes').get('count')))
    rt.append(long(jo.get('comments').get('count')))
    rt.append(jo.get('user').get('id'))
    rt.append(jo.get('caption').get('text'))
    #rt.append(jo.get('tags').get('low_resolution').get('url'))
    rt.append(jo.get('tags'))

    #try to add location if it exists
    try:
        rt.append(jo.get('location').get('longitude'))
    except Exception:
        rt.append("null")                                            
    
    try:
        rt.append(jo.get('location').get('latitude'))
    except Exception:
        rt.append("null")

    return rt

def get_insta_post_row(post):
    """Return Row of post"""
    
    return Row(id=post[0], time=datetime.fromtimestamp(post[1]),
               likes=post[2], \
                   comments=post[3], user=post[4], caption=post[5], \
                   tags=post[6], latitude=post[7], longitude=post[8])


conf = SparkConf().setAppName('Parquet Combine All... this will hopefully not take forever')
sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)


location_folder  = "/user/saifc/geo_tags_1"
posts_folder = "/user/saifc/big_files"
user_folder = "/user/saifc/user_data"

insta_posts = sc.textFile(posts_folder)
insta_posts = insta_posts.map(json_loads2)
insta_posts = insta_posts.filter(lambda (jo, status): status == 1)
insta_posts = insta_posts.map(lambda (jo, status): jo)
insta_posts = insta_posts.filter(error_jo).cache() 

#filter out dates for 1 year
insta_posts = insta_posts.filter(lambda jo: (int(jo.get("caption").get("created_time")) >= 1412899200))

insta_posts = insta_posts.filter(lambda jo: (int(jo.get("caption").get("created_time")) <= 1444435199)).cache()

insta_posts = insta_posts.map(json_to_list).cache()

#convert insta_posts to row object so we can insert them into a df
insta_posts = insta_posts.map(get_insta_post_row).cache()

#convert to df!
#posts_schema = ["id", "date", "likes", "comments", "user", "text", "tags"]
insta_posts_df = sqlContext.createDataFrame(insta_posts)

# get posts by day

#by_date = insta_posts.map(lambda row: (str(row.time.date()), 1))
#by_date = by_date.reduceByKey(operator.add).coalesce(1)
#by_date = by_date.sort(lambda (d, n): -1 * n)

#print (by_date.take(10))

#by_date.saveAsTextFile("/user/saifc/by_day4")



## Read in location data ##
geo_data = sqlContext.read.json(location_folder) #read
geo_data_df = geo_data.dropDuplicates().cache() #drop dup

# Read in user data
user_data = sqlContext.read.json(user_folder)
user_data_df = user_data.dropDuplicates().cache()
user_data_df = user_data_df.select(user_data_df.id,\
                   user_data_df.counts.followed_by.alias("followed"),\
                   user_data_df.counts.follows.alias("follows")).cache()

## join all three! ##
posts_with_geo = insta_posts_df.join(geo_data_df, geo_data_df.id == insta_posts_df.id)\
    .select(insta_posts_df.caption, insta_posts_df.comments, insta_posts_df.id, \
            insta_posts_df.latitude, insta_posts_df.longitude, insta_posts_df.likes, \
            insta_posts_df.tags, insta_posts_df.time, insta_posts_df.user, \
            geo_data_df.Location.city.alias("city"), \
            geo_data_df.Location.metro.alias("metro"),\
            geo_data_df.Location.country.alias("country")).cache()
#posts_with_geo.printSchema()
final_df = posts_with_geo.join(user_data_df,\
                     posts_with_geo.user == user_data_df.id).select(\
                     posts_with_geo.caption,\
                     posts_with_geo.comments,\
                     posts_with_geo.id,\
                     posts_with_geo.latitude,\
                     posts_with_geo.longitude,\
                     posts_with_geo.likes,\
                     posts_with_geo.tags,\
                     posts_with_geo.time,\
                     posts_with_geo.user,\
                     posts_with_geo.city,\
                     posts_with_geo.metro,\
                     posts_with_geo.country,\
                     user_data_df.follows,\
                     user_data_df.followed).cache()

final_df.printSchema()

## save to parquet! ##

final_df.write.save("FINAL_PROJECT_PARQUET", format="parquet")
