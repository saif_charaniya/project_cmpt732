# Preform quality analysis of chocolate brands by investingating the captions
# of instagram posts.  Count the number of times the brand name is used, 
# number of likes, comments, number of people who follow the user, ratio of 
# likes to followers, number of posts where they are mentioned, and top 
# countries where they are mentioned.  (Added later:) find variance of the 
# above sums use mllib vectors, which can be used to fin the sums too!

# This was actually run locally and not on the cluster bc the cluster was 
# busy with programs submitted by others :(

from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.mllib.stat import Statistics
import operator

def list_to_csv(l):
    """Return csv string format for list """
    rt = ""
    for item in l:
        rt += str(item) + ","
    rt = rt[:-1]
    return rt

def add_tuples(a, b):
    return tuple(sum(p) for p in zip(a,b))

def preform_tasks(brand, rdd):
    """ sequentially perform the tasks required for this filter rdd of one
    brand.  rdd: (caption, likes, comments, followers, country)
    Return a list in this format:
    [brand, num_posts, avg_likes, avg_comments, avg_followers, avg_occurance, 
          avg_likes/avg_followers, top_country, top_country_count,
          top_country2, top_country2_count, 
          top_country3, top_country3_count]; update: return 3 separate lists
          for averages, variances, and countries"""
    
    #map to: [likes, comments, followers, occurance]
    rdd_1 = rdd.map(lambda tup: [tup[1], tup[2], tup[3],\
                                tup[0].count(brand)]).cache()

    #perform counts
    summary = Statistics.colStats(rdd_1)
    N = summary.count()
    counts = [brand, N]
    counts.extend(summary.mean().tolist())
    vari = [brand].extend((1.0 * summary.variance() / N).tolist())
OB   
    #now find top countries (country, 1)
    rdd_2 = rdd.map(lambda tup: (tup[4], 1)).cache()
    rdd_2 = rdd_2.filter(lambda (c, N): c != "null").cache()
    rdd_2 = rdd_2.reduceByKey(operator.add)
    rdd_2 = rdd_2.sortBy(lambda (country, N): -1 * N)
    sum_2 = rdd_2.map(lambda (country, N): ("sum", N)).cache()
    sum_2 = sum_2.reduceByKey(operator.add)
    sum_2 = sum_2.take(1)[0][1]
    
    top4 = rdd_2.take(4)
    country_list = []
    for tup in top4:
        country_list.extend([tup[0], tup[1]])
    country_list.extend(["total", sum_2])    
    
    return counts, vari, country_list

def main(parquet_location):

    conf = SparkConf().setAppName('Chocolate Brand Value Analysis')
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)

    #read in parquet                                                           
    data = sqlContext.read.parquet(parquet_location)

    #these are the brands to analyze
    brands = ["godiva", "lindt", "hershey", "oreo", "nutella", "ferrero", \
                  "mars", "snickers", "m&ms", "reese", "kitkat", "cadbury",\
                  "milka", "galaxy", "toblerone", "patchi", "guylian", \
                  "ghirardelli"]

    #output lists
    final_avgs = []
    final_variance = []
    final_countries = []

    
    #get rdd of need columns
    rdd = data.select(data.caption, data.likes,\
                     data.comments, data.followed, data.country).rdd.cache()
    
    #row to tuple
    rdd = rdd.map(lambda row: (row.caption.lower(), row.likes,\
                     row.comments, row.followed, row.country)).cache()
    
    for brand in brands:
        #filter brand in caption:
        brand_rdd = rdd.filter(lambda tup: tup[0].count(brand) > 0).cache()

        #test fundction
        avg, var, country = preform_tasks(brand, brand_rdd)
    
        print("LIST ", avg)
        print("LIST ", var)
        print("LIST ", country)


        #add to final_list
        final_avgs.append(avg)
        final_variance.append(var)
        final_countries.append(country)

    #save final_list
    final_avgs = sc.parallelize(final_avgs).coalesce(1)
    final_variance = sc.parallelize(final_variance).coalesce(1)
    final_countries = sc.parallelize(final_countries).coalesce(1)

    #convert to csv format for easy input to other python scripts:
    final_avgs = final_avgs.map(list_to_csv)
    final_variance = final_variance.map(list_to_csv)
    final_countries = final_countries.map(list_to_csv)

    #save to files
    final_avgs.saveAsTextFile("brand_analysis/averages/")   
    final_variance.saveAsTextFile("brand_analysis/variances/")
    final_countries.saveAsTextFile("brand_analysis/countries/")   

if __name__ == "__main__":

    main("FINAL_PROJECT_PARQUET")
