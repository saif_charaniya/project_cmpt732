#find top posting countries for vday

from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.mllib.stat import Statistics
import operator, sys
from pyspark.mllib.fpm import FPGrowth

def add_tuples(a, b):
    return tuple(sum(p) for p in zip(a,b))

def main(sc, parquet_location, output):
    '''Find top posting countries on vday'''
    
    sqlContext = SQLContext(sc)

    #read in parquet                                                           
    data = sqlContext.read.parquet(parquet_location)
    
    #print data.show()
    

    data = data.select(data.tags, data.time).rdd.cache()
    data = data.map(lambda row: (row.tags, row.time.strftime('%Y-%m-%d'), \
                                     )).cache()
    
    #find top hashtags in world
    data = data.filter(lambda (h, t): t=="2015-02-14")\
        .map(lambda (h,t):h).cache()
    
    #freqitemsets
    model = FPGrowth.train(data, 0.001)
    itemsets = model.freqItemsets().cache()

    itemsets = itemsets.map(lambda freq_set:\
                                (sorted(freq_set[0]), freq_set[1]))
    itemsets = itemsets.sortBy(lambda(itemset, freq): itemset)
    itemsets = itemsets.sortBy((lambda (itemset, freq) : -freq)).cache()

    size1 = itemsets.filter(lambda (itemset, freq): len(itemset) == 1).cache()
    size1 = sc.parallelize(size1.take(100)).coalesce(1)

    size1.saveAsTextFile(output + "s1")
    
    size3 = itemsets.filter(lambda (itemset, freq): len(itemset) == 3).cache()
    size3 = sc.parallelize(size3.take(100))
    size3.saveAsTextFile(output + "s3")

    
if __name__ == "__main__":
    
    conf = SparkConf().setAppName('VDAY Hashtags')
    sc = SparkContext(conf=conf)
    loc = sys.argv[1]
    output = sys.argv[2]
    main(sc, loc, output)
