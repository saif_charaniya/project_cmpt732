from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.mllib.stat import Statistics
import operator, sys

def add_tuples(a, b):
    return tuple(sum(p) for p in zip(a,b))

def main(sc, parquet_location):
    '''Find top posting countries on vday'''
    
    sqlContext = SQLContext(sc)

    #read in parquet                                                           
    data = sqlContext.read.parquet(parquet_location)

    data = data.select(data.likes, data.user, data.time, data.country)\
        .rdd.cache()
    data = data.map(lambda row: (row.user, row.time.strftime('%Y-%m-%d'), \
                                     row.likes, row.country)).cache()
    data = data.filter(lambda (u,t,l,c): t=="2015-02-14" and c=="CA")\
        .map(lambda (u,t,l,c):(u,l)).cache()
    print data.takeOrdered(3, key=lambda (u,l): -l)
    

if __name__ == "__main__":
    
    conf = SparkConf().setAppName('VDAY CA Top Post')
    sc = SparkContext(conf=conf)
    loc = sys.argv[1]
    main(sc, loc)
