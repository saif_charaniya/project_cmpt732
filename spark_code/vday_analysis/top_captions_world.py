#find top posting countries for vday

from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.mllib.stat import Statistics
import operator, sys
from pyspark.mllib.fpm import FPGrowth
import nltk
from nltk.corpus import stopwords
import string

def add_tuples(a, b):
    return tuple(sum(p) for p in zip(a,b))

def string_review(line, stop_words):
   ''' Replace all punctuation, convert to lowercase, split into words,        
   and remove stopwords, return list of final words'''

   #encode so we can use translate                                             
   line = line.encode('utf-8')

   line = line.translate(string.maketrans("",""), string.punctuation)

   #decode for non english                                                     
   line = line.decode('utf-8').lower()
   #remove stopwords:                                                          
   final_words = [i for i in line.split() if i not in stop_words]

   final_2 = []
   for i in final_words:
       try:
           final_2.index(i)
       except:
           final_2.append(i)

   return final_2

def main(sc, parquet_location, output, nltk_loc):
    '''Find top posting countries on vday'''
    
    sqlContext = SQLContext(sc)

    #nltk 
    #load stopwords                                                            
    nltk.data.path.append(nltk_loc)
    stop_words = set(stopwords.words("english"))


    #read in parquet                                                           
    data = sqlContext.read.parquet(parquet_location)
    
    #print data.show()
    

    data = data.select(data.caption, data.time).rdd.cache()
    data = data.map(lambda row: (row.caption, row.time.strftime('%Y-%m-%d'), \
                                     )).cache()
    
    #find top hashtags in world
    data = data.filter(lambda (h, t): t=="2015-02-14")\
        .map(lambda (h,t):string_review(h, stop_words)).cache()
    
    #freqitemsets
    model = FPGrowth.train(data, 0.001)
    itemsets = model.freqItemsets().cache()

    itemsets = itemsets.map(lambda freq_set:\
                                (sorted(freq_set[0]), freq_set[1]))
    itemsets = itemsets.sortBy(lambda(itemset, freq): itemset)
    itemsets = itemsets.sortBy((lambda (itemset, freq) : -freq)).cache()

    size3 = itemsets.filter(lambda (itemset, freq): len(itemset) == 5).cache()
    size3 = sc.parallelize(size3.take(100)).coalesce(1)

    size3.saveAsTextFile(output + "s3")
    
    size5 = itemsets.filter(lambda (itemset, freq): len(itemset) == 5).cache()
    size5 = sc.parallelize(size5.take(100))
    size5.saveAsTextFile(output + "s5")

    
if __name__ == "__main__":
    
    conf = SparkConf().setAppName('VDAY Hashtags')
    sc = SparkContext(conf=conf)
    loc = sys.argv[1]
    output = sys.argv[2]
    nltk_loc = sys.argv[3]
    main(sc, loc, output, nltk_loc)
