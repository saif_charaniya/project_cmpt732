#find top posting countries for vday

from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.mllib.stat import Statistics
import operator, sys

def add_tuples(a, b):
    return tuple(sum(p) for p in zip(a,b))

def main(sc, parquet_location, output):
    '''Find top posting countries on vday'''
    
    sqlContext = SQLContext(sc)

    #read in parquet                                                           
    data = sqlContext.read.parquet(parquet_location)
    
    #print data.show()
    

    data = data.select(data.time, data.country, data.metro).rdd.cache()
    data = data.map(lambda row: (row.country, row.metro, \
                                     row.time.strftime('%Y-%m-%d'))).cache()
    data = data.filter(lambda (c,m,t): c=="CA")\
        .map(lambda (c,m,t): (m,t)).cache()
    
    #average number of posts per country per day:
    sums = data
    sums = sums.map(lambda (c, d): ((c,d) , 1)).reduceByKey(operator.add).cache()
    sums = sums.map(lambda ((c,d), n): (c, (n, 1))).reduceByKey(add_tuples).cache()
    
    sums = sums.map(lambda (c, (n, d)): (c, (1.0 * n)/d))
        #.map(lambda (c, ag): str(c) + ";" + str(ag)).coalesce(1).cache()
    
    #sums.saveAsTextFile("average_posts_per_day_country")

    #number of posts on vday
    vposts = data
    vposts = vposts.filter(lambda (c, d): d == "2015-02-14").cache()
    vposts = vposts.map(lambda (c, d): (c, 1))\
        .reduceByKey(operator.add).cache()
    
    #a joined rdd of both average and vday sums
    final = sums.join(vposts).cache()
    final = final.map(lambda (c, (av, vd)): (c, av, vd, (100.0 * (vd-av))/av))
        #.coalesce(1).cache()
    final = sc.parallelize(final.takeOrdered(100, key=lambda (a,b,c,d): -d))\
        .map(lambda (a,b,c,d): str(a) + "," + str(b) + "," + str(c) + "," \
                 + str(d)).coalesce(1).cache()
    final.saveAsTextFile(output)
    

if __name__ == "__main__":
    
    conf = SparkConf().setAppName('VDAY 1')
    sc = SparkContext(conf=conf)
    loc = sys.argv[1]
    output = sys.argv[2]
    main(sc, loc, output)
