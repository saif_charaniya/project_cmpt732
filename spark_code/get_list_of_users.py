## 
## This file will create a list of user ids from instagram posts and save
## it to a single file. That file will be used later with the Instagram API
## to get the user data. This program will also tally the number of posts, and
## calculate the average posts per user in the one year span from 
## oct 10 2014 to oct 9 2015
##
##

#imports 
from pyspark import SparkConf, SparkContext
import sys, operator
import json

## Functions
def add_pairs ((a1, b1), (a2, b2)):
    """ Add two pairs of tuples by index """
    return (a1 + a2, b1 + b2)

def json_loads2(line):
    """load a json, discard if not proper"""
    try:
        jo = json.loads(line)
        if type(jo) is dict:
            return (jo, 1)
    except :#ValueError:
        print ("BADLINE:", line)
        return ("EMPTY", -1)

def error (jo):
    """Check if the json has caption and created_time"""
    try:
        jo.get('caption').get('created_time')
        return True
    except:
        print ("ERRR")
        return False

## main block

conf = SparkConf().setAppName('Get All Users')
sc = SparkContext(conf=conf)

if (len(sys.argv) != 3):
    raise SystemExit

input_folder = sys.argv[1]
output_folder = sys.argv[2]

insta_posts = sc.textFile(input_folder)

insta_posts = insta_posts.map(json_loads2)
insta_posts = insta_posts.filter(lambda (jo, status): status == 1)
insta_posts = insta_posts.map(lambda (jo, status): jo)
insta_posts = insta_posts.filter(error)

#filter out dates
insta_posts = insta_posts.filter(lambda jo: (int(jo.get("caption").get("created_time")) >= 1412899200))

insta_posts = insta_posts.filter(lambda jo: (int(jo.get("caption").get("created_tim\
e")) <= 1444435199)).cache()

insta_posts.saveAsTextFile(output_folder)

insta_posts = insta_posts.map(lambda jo: (jo.get('user').get('id'), 1)).cache()
insta_posts = insta_posts.reduceByKey(operator.add).cache()
users = insta_posts.map(lambda (id, count): id).coalesce(1)

#save users
users.saveAsTextFile(output_folder + "/user_ids")

#find average
average = insta_posts.map(lambda (id, count): ("avg", (1, count)))
average = average.reduceByKey(add_pairs)
average = average.map(lambda (id, pair): pair).cache()

sums = average.take(1)

num_ids  = sums[0][0]
num_posts = sums[0][1]
avg = 1.0 * num_posts / num_ids

output_rdd = sc.parallelize([("Num Users", num_ids), ("Num posts", num_posts), ("avg posts per user", avg)])

#save averages
output_rdd.saveAsTextFile(output_folder + "/averages")
