# get number of posts, post performance, number of likes, number of comments,
# each per day for all the countries.  This will be save in one file
#  which will can be used to plot the results

#imports
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
import sys  

#functions
def add_tuples(a, b):
    return tuple(sum(p) for p in zip(a,b))

def post_performance(likes, comments, followedby):
    """Return this post's performance"""
    return (90.0 * likes / followedby) + (10.0 * comments / followedby)
    #return (1.0 * likes) / followedby

def main(parquet_location, output):

    conf = SparkConf().setAppName('Daily Stats')
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)

    #read in parquet
    data = sqlContext.read.parquet(parquet_location)

    #get needed colums into a rdd!
    data = data.select(data.time, data.likes, \
                           data.comments, data.followed).rdd.cache()
    
    #kv pairs for likes and comments
    bydate = data.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                                       (1, row.likes, row.comments)))
    
    #create kv pairs, calc post performance
    pp = data.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                      (row.likes, row.comments, row.followed)))
    pp = pp.filter(lambda (t, (l, c, f)): f > 1)
    pp = pp.map(lambda (t, (l, c, f)): \
                    (t, (1, post_performance(l, c, f)))).cache()
    
    #reduce to add scores 
    bydate = bydate.reduceByKey(add_tuples).cache()
    pp = pp.reduceByKey(add_tuples).cache()

    #set averages 
    bydate = bydate.map(lambda (date, (n, l, c)): (date, ( n, 1.0 * l / n, \
                                   1.0 * c / n)))
    pp = pp.map(lambda (date, (n, p)): (date, 1.0 * p / n))

    #join the two
    final = bydate.join(pp)
    final = final.map(lambda (date, ((n, l, c), pp)): \
                     date + ";" + str(n) + ";" + str(l) + ";"\
                          + str(c) + ";" + str(pp))
    final = final.coalesce(1)

    #save to output
    final.saveAsTextFile(output + "/nlcp_byday2/")

if __name__ == "__main__":

    main("FINAL_PROJECT_PARQUET", "analysis_posts")
    
