# get number of posts, post performance, number of likes, number of comments,
# each per day for 7 selected countries.  Each country's data will be saved
#  in one file each.  This will be outputted to folders by the country names.
#  Plots will be made for each of these as in the analysis for all countries

#imports
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
import sys  

#functions
def add_tuples(a, b):
    return tuple(sum(p) for p in zip(a,b))

def post_performance(likes, comments, followedby):
    """Return this post's performance"""
    return (90.0 * likes / followedby) + (10.0 * comments / followedby)

def main(parquet_location, output):

    conf = SparkConf().setAppName('Daily Stats')
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)

    #read in parquet
    data = sqlContext.read.parquet(parquet_location)

    #get needed colums into a rdd!  (includes country)
    data = data.select(data.time, data.likes, \
                    data.comments, data.followed, data.country).rdd.cache()
    
    #Filter for 7 countries each having their own rdds
    canada = data.filter(lambda row: row.country == 'CA')
    usa = data.filter(lambda row: row.country == 'US')
    brazil = data.filter(lambda row: row.country == 'BR')
    china = data.filter(lambda row: row.country == 'CN')
    russia = data.filter(lambda row: row.country == 'RU')
    india = data.filter(lambda row: row.country == 'IN')
    turkey = data.filter(lambda row: row.country == 'TR')

    ##perform the steps for each country one by one, save at the end
    ## country_bydate, country_pp, country_final : 3 for each country
    
    #CANADA 
    #kv pairs for likes and comments
    canada_bydate = canada.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                                       (1, row.likes, row.comments)))
    
    #create kv pairs, calc post performance
    canada_pp = canada.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                      (row.likes, row.comments, row.followed)))
    canada_pp = canada_pp.filter(lambda (t, (l, c, f)): f > 1)
    canada_pp = canada_pp.map(lambda (t, (l, c, f)): \
                    (t, (1, post_performance(l, c, f)))).cache()
    
    #reduce to add scores 
    canada_bydate = canada_bydate.reduceByKey(add_tuples).cache()
    canada_pp = canada_pp.reduceByKey(add_tuples).cache()

    #set averages 
    canada_bydate = canada_bydate.map(lambda (date, (n, l, c)):\
                   (date, ( n, 1.0 * l / n, 1.0 * c / n)))
    canada_pp = canada_pp.map(lambda (date, (n, p)): (date, 1.0 * p / n))

    #join the two
    canada_final = canada_bydate.join(canada_pp)
    canada_final = canada_final.map(lambda (date, ((n, l, c), pp)): \
                     date + ";" + str(n) + ";" + str(l) + ";"\
                          + str(c) + ";" + str(pp))
    canada_final = canada_final.coalesce(1)

    #usa                                                                   
    #kv pairs for likes and comments                                           
    usa_bydate = usa.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                                       (1, row.likes, row.comments)))
    #create kv pairs, calc post performance                                    
    usa_pp = usa.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                      (row.likes, row.comments, row.followed)))
    usa_pp = usa_pp.filter(lambda (t, (l, c, f)): f > 1)
    usa_pp = usa_pp.map(lambda (t, (l, c, f)): \
                    (t, (1, post_performance(l, c, f)))).cache()

    #reduce to add scores                                                      
    usa_bydate = usa_bydate.reduceByKey(add_tuples).cache()
    usa_pp = usa_pp.reduceByKey(add_tuples).cache()

    #set averages                                                              
    usa_bydate = usa_bydate.map(lambda (date, (n, l, c)):\
                   (date, ( n, 1.0 * l / n, 1.0 * c / n)))
    usa_pp = usa_pp.map(lambda (date, (n, p)): (date, 1.0 * p / n))

    #join the two                                                              
    usa_final = usa_bydate.join(usa_pp)
    usa_final = usa_final.map(lambda (date, ((n, l, c), pp)): \
                     date + ";" + str(n) + ";" + str(l) + ";"\
                          + str(c) + ";" + str(pp))
    usa_final = usa_final.coalesce(1)

    #brazil
    #kv pairs for likes and comments                                           
    brazil_bydate = brazil.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                                       (1, row.likes, row.comments)))
    #create kv pairs, calc post performance                                    
    brazil_pp = brazil.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                      (row.likes, row.comments, row.followed)))
    brazil_pp = brazil_pp.filter(lambda (t, (l, c, f)): f > 1)
    brazil_pp = brazil_pp.map(lambda (t, (l, c, f)): \
                    (t, (1, post_performance(l, c, f)))).cache()

    #reduce to add scores                                                      
    brazil_bydate = brazil_bydate.reduceByKey(add_tuples).cache()
    brazil_pp = brazil_pp.reduceByKey(add_tuples).cache()

    #set averages                                                              
    brazil_bydate = brazil_bydate.map(lambda (date, (n, l, c)):\
                   (date, ( n, 1.0 * l / n, 1.0 * c / n)))
    brazil_pp = brazil_pp.map(lambda (date, (n, p)): (date, 1.0 * p / n))

    #join the two                                                              
    brazil_final = brazil_bydate.join(brazil_pp)
    brazil_final = brazil_final.map(lambda (date, ((n, l, c), pp)): \
                     date + ";" + str(n) + ";" + str(l) + ";"\
                          + str(c) + ";" + str(pp))
    brazil_final = brazil_final.coalesce(1)
    
    #CHINA
    #kv pairs for likes and comments                                           
    china_bydate = china.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                                       (1, row.likes, row.comments)))
    #create kv pairs, calc post performance                                    
    china_pp = china.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                      (row.likes, row.comments, row.followed)))
    china_pp = china_pp.filter(lambda (t, (l, c, f)): f > 1)
    china_pp = china_pp.map(lambda (t, (l, c, f)): \
                    (t, (1, post_performance(l, c, f)))).cache()

    #reduce to add scores                                                      
    china_bydate = china_bydate.reduceByKey(add_tuples).cache()
    china_pp = china_pp.reduceByKey(add_tuples).cache()

    #set averages                                                              
    china_bydate = china_bydate.map(lambda (date, (n, l, c)):\
                   (date, ( n, 1.0 * l / n, 1.0 * c / n)))
    china_pp = china_pp.map(lambda (date, (n, p)): (date, 1.0 * p / n))

    #join the two                                                              
    china_final = china_bydate.join(china_pp)
    china_final = china_final.map(lambda (date, ((n, l, c), pp)): \
                     date + ";" + str(n) + ";" + str(l) + ";"\
                          + str(c) + ";" + str(pp))
    chian_final = china_final.coalesce(1)

    #INDIA
    #kv pairs for likes and comments                                           
    india_bydate = india.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                                       (1, row.likes, row.comments)))
    #create kv pairs, calc post performance                                    
    india_pp = india.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                      (row.likes, row.comments, row.followed)))
    india_pp = india_pp.filter(lambda (t, (l, c, f)): f > 1)
    india_pp = india_pp.map(lambda (t, (l, c, f)): \
                    (t, (1, post_performance(l, c, f)))).cache()

    #reduce to add scores                                                      
    india_bydate = india_bydate.reduceByKey(add_tuples).cache()
    india_pp = india_pp.reduceByKey(add_tuples).cache()

    #set averages                                                              
    india_bydate = india_bydate.map(lambda (date, (n, l, c)):\
                   (date, ( n, 1.0 * l / n, 1.0 * c / n)))
    india_pp = india_pp.map(lambda (date, (n, p)): (date, 1.0 * p / n))

    #join the two                                                              
    india_final = india_bydate.join(india_pp)
    india_final = india_final.map(lambda (date, ((n, l, c), pp)): \
                     date + ";" + str(n) + ";" + str(l) + ";"\
                          + str(c) + ";" + str(pp))
    india_final = india_final.coalesce(1)
    
    #RUSSIA
    #kv pairs for likes and comments                                           
    russia_bydate = russia.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                                       (1, row.likes, row.comments)))
    #create kv pairs, calc post performance                                    
    russia_pp = russia.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                      (row.likes, row.comments, row.followed)))
    russia_pp = russia_pp.filter(lambda (t, (l, c, f)): f > 1)
    russia_pp = russia_pp.map(lambda (t, (l, c, f)): \
                    (t, (1, post_performance(l, c, f)))).cache()

    #reduce to add scores                                                      
    russia_bydate = russia_bydate.reduceByKey(add_tuples).cache()
    russia_pp = russia_pp.reduceByKey(add_tuples).cache()

    #set averages                                                              
    russia_bydate = russia_bydate.map(lambda (date, (n, l, c)):\
                   (date, ( n, 1.0 * l / n, 1.0 * c / n)))
    russia_pp = russia_pp.map(lambda (date, (n, p)): (date, 1.0 * p / n))

    #join the two                                                              
    russia_final = russia_bydate.join(russia_pp)
    russia_final = russia_final.map(lambda (date, ((n, l, c), pp)): \
                     date + ";" + str(n) + ";" + str(l) + ";"\
                          + str(c) + ";" + str(pp))
    russia_final = russia_final.coalesce(1)

    #TURKEY
    #kv pairs for likes and comments                                           
    turkey_bydate = turkey.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                                       (1, row.likes, row.comments)))
    #create kv pairs, calc post performance                                    
    turkey_pp = turkey.map(lambda row: (row.time.strftime('%Y-%m-%d'),\
                      (row.likes, row.comments, row.followed)))
    turkey_pp = turkey_pp.filter(lambda (t, (l, c, f)): f > 1)
    turkey_pp = turkey_pp.map(lambda (t, (l, c, f)): \
                    (t, (1, post_performance(l, c, f)))).cache()

    #reduce to add scores                                                      
    turkey_bydate = turkey_bydate.reduceByKey(add_tuples).cache()
    turkey_pp = turkey_pp.reduceByKey(add_tuples).cache()

    #set averages                                                              
    turkey_bydate = turkey_bydate.map(lambda (date, (n, l, c)):\
                   (date, ( n, 1.0 * l / n, 1.0 * c / n)))
    turkey_pp = turkey_pp.map(lambda (date, (n, p)): (date, 1.0 * p / n))

    #join the two                                                              
    turkey_final = turkey_bydate.join(turkey_pp)
    turkey_final = turkey_final.map(lambda (date, ((n, l, c), pp)): \
                     date + ";" + str(n) + ";" + str(l) + ";"\
                          + str(c) + ";" + str(pp))
    turkey_final = turkey_final.coalesce(1)

    ## SAVE ALL RDDS
    # save to output folder / country / by day
    canada_final.saveAsTextFile(output + "/canada/by_day/")
    usa_final.saveAsTextFile(output + "/usa/by_day/")
    russia_final.saveAsTextFile(output + "/russia/by_day/")
    brazil_final.saveAsTextFile(output + "/brazil/by_day/")
    india_final.saveAsTextFile(output + "/india/by_day/")
    china_final.saveAsTextFile(output + "/china/by_day/")
    turkey_final.saveAsTextFile(output + "/turkey/by_day/")

if __name__ == "__main__":

    main("FINAL_PROJECT_PARQUET", "analysis_posts")
