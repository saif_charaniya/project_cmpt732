# Find the frequent itemsets through hashtags of all instagram posts
# from the top 4 countries: US, BR, IT, and AU, and CA (for fun)
# using Spark ML libraries, in paticular, FP Growth
# In paticular, we want the top few itemsets in multiple sizes (1 to 4 ish)
# save these for later analysis.  

#imports                                                                       
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.mllib.fpm import FPGrowth

def main(parquet_location):

    conf = SparkConf().setAppName('Top Countries Frequent Itemsets Stats')
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)

    #read in parquet                                                          
    data = sqlContext.read.parquet(parquet_location)

    #get list of hashtags in an rdd
    hashtags = data.select(data.tags, data.country).rdd
    
    #map to pair of list and country
    hashtags = hashtags.map(lambda row: (row.tags, row.country)).cache()


    #separate by country
    hash_us = hashtags.filter(lambda (tags, cont): cont=="US")
    hash_us = hash_us.map(lambda (tags, cont): tags).cache()

    hash_br = hashtags.filter(lambda (tags, cont): cont=="BR")
    hash_br = hash_br.map(lambda (tags,cont): tags).cache()
                       
    hash_it = hashtags.filter(lambda (tags, cont): cont=="IT")
    hash_it = hash_it.map(lambda (tags,cont): tags).cache()

    hash_au = hashtags.filter(lambda (tags, cont): cont=="AU")
    hash_au = hash_au.map(lambda (tags,cont): tags).cache()

    hash_ca = hashtags.filter(lambda (tags, cont): cont=="CA")
    hash_ca = hash_ca.map(lambda (tags,cont): tags).cache()

    #find freq itemsets per country!
    
    

    #perform frequent itemsets support of 1 %
    model_us = FPGrowth.train(hash_us, 0.01)
    model_br = FPGrowth.train(hash_br, 0.01)
    model_it = FPGrowth.train(hash_it, 0.01)
    model_au = FPGrowth.train(hash_au, 0.01)
    model_ca = FPGrowth.train(hash_ca, 0.01)

    #itemsets = sc.parallelize(model.freqItemsets().collect()).cache()
    itemsets_us = model_us.freqItemsets().cache()
    itemsets_br = model_br.freqItemsets().cache()
    itemsets_it = model_it.freqItemsets().cache()
    itemsets_au = model_au.freqItemsets().cache()
    itemsets_ca = model_ca.freqItemsets().cache()
    
    #sort lexiograpphically and by freq
    itemsets_us = itemsets_us.map(lambda freq_set:\
                                (sorted(freq_set[0]), freq_set[1]))
    itemsets_us = itemsets_us.sortBy(lambda(itemset, freq): itemset)
    itemsets_us = itemsets_us.sortBy((lambda(itemset, freq) : freq)\
                                         , False).cache()
    
    itemsets_br = itemsets_br.map(lambda freq_set:\
                                (sorted(freq_set[0]), freq_set[1]))
    itemsets_br = itemsets_br.sortBy(lambda(itemset, freq): itemset)
    itemsets_br = itemsets_br.sortBy((lambda(itemset, freq) : freq)\
                                         , False).cache()

    itemsets_it = itemsets_it.map(lambda freq_set:\
                                (sorted(freq_set[0]), freq_set[1]))
    itemsets_it = itemsets_it.sortBy(lambda(itemset, freq): itemset)
    itemsets_it = itemsets_it.sortBy((lambda(itemset, freq) : freq)\
                                         , False).cache()

    itemsets_au = itemsets_au.map(lambda freq_set:\
                                (sorted(freq_set[0]), freq_set[1]))
    itemsets_au = itemsets_au.sortBy(lambda(itemset, freq): itemset)
    itemsets_au = itemsets_au.sortBy((lambda(itemset, freq) : freq)\
                                         , False).cache()

    itemsets_ca = itemsets_ca.map(lambda freq_set:\
                                (sorted(freq_set[0]), freq_set[1]))
    itemsets_ca = itemsets_ca.sortBy(lambda(itemset, freq): itemset)
    itemsets_ca = itemsets_ca.sortBy((lambda(itemset, freq) : freq)\
                                         , False).cache()

    #split by sizes
    size1_us = itemsets_us.filter(lambda (itemset, freq):\
                                   len(itemset) == 1).cache()
    size1_us = sc.parallelize(size1_us.take(100))
    size1_us = size1_us.map(lambda (itemset, freq):\
                  str(itemset[0].encode("ascii","ignore")) + ";" + str(freq))
    
    size1_br = itemsets_br.filter(lambda (itemset, freq):\
                                   len(itemset) == 1).cache()
    size1_br = sc.parallelize(size1_br.take(100))
    size1_br = size1_br.map(lambda (itemset, freq):\
              str(itemset[0].encode("ascii","ignore")) + ";" + str(freq))

    size1_it = itemsets_it.filter(lambda (itemset, freq):\
                                   len(itemset) == 1).cache()
    size1_it = sc.parallelize(size1_it.take(100))
    size1_it = size1_it.map(lambda (itemset, freq):\
                str(itemset[0].encode("ascii","ignore")) + ";" + str(freq))

    size1_au = itemsets_au.filter(lambda (itemset, freq):\
                                   len(itemset) == 1).cache()
    size1_au = sc.parallelize(size1_au.take(100))
    size1_au = size1_au.map(lambda (itemset, freq):\
                  str(itemset[0].encode("ascii","ignore")) + ";" + str(freq))

    size1_ca = itemsets_ca.filter(lambda (itemset, freq):\
                                   len(itemset) == 1).cache()
    size1_ca = sc.parallelize(size1_ca.take(100))
    size1_ca = size1_ca.map(lambda (itemset, freq):\
                 str(itemset[0].encode("ascii","ignore")) + ";" + str(freq))
    #save files
    size1_us.saveAsTextFile("insta_fp/us/s1/")
    size1_br.saveAsTextFile("insta_fp/br/s1/")
    size1_it.saveAsTextFile("insta_fp/it/s1/")
    size1_au.saveAsTextFile("insta_fp/au/s1/")
    size1_ca.saveAsTextFile("insta_fp/ca/s1/")

if __name__ == "__main__":

    main("FINAL_PROJECT_PARQUET")
