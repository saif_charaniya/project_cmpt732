# Find the frequent itemsets through hashtags of all instagram posts using
# Spark ML libraries, in paticular, FP Growth
# In paticular, we want the top few itemsets in multiple sizes (1 to 4 ish)
# save these for later analysis.  

#imports                                                                       
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.mllib.fpm import FPGrowth

def main(parquet_location, output):

    conf = SparkConf().setAppName('Global Frequent Itemsets Stats')
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)

    #read in parquet                                                          
    data = sqlContext.read.parquet(parquet_location)

    #get list of hashtags in an rdd
    hashtags = data.select(data.tags).rdd
    
    #map to list
    hashtags = hashtags.map(lambda row: row.tags).cache()

    #perform frequent itemsets support of 1 %
    model = FPGrowth.train(hashtags, 0.01)
    #itemsets = sc.parallelize(model.freqItemsets().collect()).cache()
    itemsets = model.freqItemsets().cache()
    #sort lexiograpphically and by freq

    itemsets = itemsets.map(lambda freq_set:\
                                (sorted(freq_set[0]), freq_set[1]))
    itemsets = itemsets.sortBy(lambda(itemset, freq): itemset)
    itemsets = itemsets.sortBy((lambda (itemset, freq) : freq), False).cache()

    #split by sizes
    size1 = itemsets.filter(lambda (itemset, freq): len(itemset) == 1).cache()
    size1 = sc.parallelize(size1.take(100))
    size1 = size1.map(lambda (itemset, freq):\
                          str(itemset[0]) + ";" + str(freq))
    size1.coalesce(1)
    
    size2 = itemsets.filter(lambda (itemset, freq): len(itemset) == 2).cache()
    size2 = sc.parallelize(size2.take(100))
    size2 = size2.map(lambda (itemset, freq):\
               str(itemset[0].encode('ascii', 'ignore')) + "-" + \
               str(itemset[1].encode('ascii', 'ignore')) + ";" + str(freq))    
    size2.coalesce(1)

    size3 = itemsets.filter(lambda (itemset, freq): len(itemset) == 3).cache()
    size3 = sc.parallelize(size3.take(100))
    size3 = size3.map(lambda (itemset, freq):\
               str(itemset[0].encode('ascii', 'ignore')) + "-" +\
               str(itemset[1].encode('ascii', 'ignore')) + "-" +
               str(itemset[2].encode('ascii', 'ignore')) + "-" +\
                          ";" + str(freq))
    size3.coalesce(1)

    size4 = itemsets.filter(lambda (itemset, freq): len(itemset) == 4).cache()
    size4 = sc.parallelize(size4.take(100))
    size4 = size4.map(lambda (itemset, freq):\
               str(itemset[0].encode('ascii', 'ignore')) + "-" +\
               str(itemset[1].encode('ascii', 'ignore')) + "-" +
               str(itemset[2].encode('ascii', 'ignore')) + "-" +\
               str(itemset[3].encode('ascii', 'ignore')) + ";" +\
                    str(freq))
    size4.coalesce(1)

    #size1.saveAsTextFile("insta_fp/size--1/")
    size2.saveAsTextFile("insta_fp/size--2/")
    size3.saveAsTextFile("insta_fp/size--3/")
    size4.saveAsTextFile("insta_fp/size--4/")


if __name__ == "__main__":

    main("FINAL_PROJECT_PARQUET", "analysis_posts")
