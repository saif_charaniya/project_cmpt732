# get number of posts, avg likes and comments, for each country. Sort this by
# number of posts, and save to a single file for later use.

#imports
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
import sys  

#functions
def add_tuples(a, b):
    return tuple(sum(p) for p in zip(a,b))

def main(parquet_location, output):

    conf = SparkConf().setAppName('Daily Stats')
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)

    #read in parquet
    data = sqlContext.read.parquet(parquet_location)

    #get needed columns into an rdd
    data = data.select(data.country, data.likes, data.comments).rdd.cache()

    #remove data with null country
    data = data.filter(lambda row: row.country != "null")

    #set (k,v) pair to: (country, (1, likes, comments))
    data = data.map(lambda row: (row.country,\
                                     (1, row.likes, row.comments))).cache()

    #reduce using add_pairs:
    data = data.reduceByKey(add_tuples).cache()

    #sort                                                                     
    data = data.sortBy(lambda (c, (n, l, com)) : -1 * n)

    #map to find averages and add ";" sep for output
    data = data.map(lambda (c, (n, l, com)): c + ";" + str(n) + ";" + \
                      str(1.0 * l / n) + ";" + str(1.0 * com / n)).coalesce(1)
    

    # save data 
    data.saveAsTextFile(output + "/country_aggregates/")

if __name__ == "__main__":

    main("FINAL_PROJECT_PARQUET", "analysis_posts")
