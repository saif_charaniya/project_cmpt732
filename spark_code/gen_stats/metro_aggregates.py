# get number of posts, avg likes and comments, for each metro. Sort this by
# number of posts, and save to a single file for later use.

#imports
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
import sys  

#functions
def add_tuples(a, b):
    return tuple(sum(p) for p in zip(a,b))

def main(parquet_location, output):

    conf = SparkConf().setAppName('Metro Stats')
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)

    #read in parquet
    data = sqlContext.read.parquet(parquet_location)

    #get needed columns into an rdd
    data = data.select(data.metro, data.likes, data.comments).rdd.cache()

    #remove data with null country
    data = data.filter(lambda row: row.metro != "null")

    #set (k,v) pair to: (country, (1, likes, comments))
    data = data.map(lambda row: (row.metro,\
                                     (1, row.likes, row.comments))).cache()

    #reduce using add_pairs:
    data = data.reduceByKey(add_tuples).cache()


    #sort                                                                      
    data = data.sortBy(lambda (m, (n,l,c)): -1 * n)

    #map to find averages and add ";" sep for output
    data = data.map(lambda (m, (n, l, c)): m + ";" + str(n) + ";" + \
                      str(1.0 * l / n) + ";" + str(1.0 * c / n)).coalesce(1)
    
    # save data 
    data.saveAsTextFile(output + "/metro_aggregates/")

if __name__ == "__main__":

    main("FINAL_PROJECT_PARQUET", "analysis_posts")
