!! View the project Website at: http://goo.gl/KZpG9T
!! View a description of the code and files and folders in RUNNING.md

Welcome to Saif Charaniya's Final Project for CMPT732 at the Simon Fraser University (2015).

Here you will find most of the code and resources used in the project.  
This repo does not include any raw data used in the project and any reports or papers written based on the conclusions.

All documents of the repository are protect by the Apache License Version 2.0. 
You can view the license in "LICENSE.txt"

If you plan to use and/or modify any documents contained within please fully credit 
and reference Saif Charaniya with a link back to this repository's page.

About:

This project will use a large data set retrieved using the Instagram API to perform a statistical 
analysis using big data tools such as Hadoop, Spark, parcquets, and MapReduce, R, python, and Matplotlib.

The project is set to conclude Dec 13th.  Come back then for a more complete story!

Credits:

Unless specifid, all code within this Repo is written by Saif Charaniya.

Much appreciation goes to Daniel Glasson and his github Reop, "OfflineReverseGeoCode" 
which was used and modified to organize Instagram posts by geographical location: https://github.com/AReallyGoodName/OfflineReverseGeocode/blob/master/src/main/java/geocode/ReverseGeoCode.java
 
Much appreciation goes to GeoNames for providing access to cities1000.txt and country_codes.csv files.

Much appreciation goes to Andreas Mueller for the python Word Cloud library: https://github.com/amueller/word_cloud

Much appreciation goes to Kerry Rodden for the D3 Sequences Sunburst library: https://gist.github.com/kerryrodden/7090426
